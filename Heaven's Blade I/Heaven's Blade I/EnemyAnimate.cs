﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;

namespace Heaven_s_Blade_I
{
    class EnemyAnimate : Enemy
    {
        // the elapsed amount of time the frame has been shown for		
        float time = 0;

        // the position of our image
        Vector2 position;

        // duration of time to show each frame
        float frameTime = 0.1f;

        // an index of the current frame being shown
        int frameIndex;

        // total number of frames in our spritesheet
        const int totalFrames = 4;

        // define the size of our animation frame
        int frameHeight = 24;
        int frameWidth = 24;

        public AnimState aState;

        public Rectangle source;

        public EnemyAnimate(int x, int y, Texture2D Image)
            : base(x, y, Image)
        {
            source = new Rectangle(0, 0, 24, 24);
        }

        public void AUpdate(KeyboardState kState, KeyboardState pkState)
        {
            if(SingleKeyPress(Keys.W, kState, pkState))
            {
                aState = AnimState.Up;
                source = new Rectangle(48, 0, frameWidth, frameHeight);
            }
            else if(SingleKeyPress(Keys.A, kState, pkState))
            {
                aState = AnimState.Left;
                source = new Rectangle(96, 0, frameWidth, frameHeight);
            }
            else if(SingleKeyPress(Keys.S, kState, pkState))
            {
                aState = AnimState.Down;
                source = new Rectangle(0, 0, frameWidth, frameHeight);
            }
            else if(SingleKeyPress(Keys.D, kState, pkState))
            {
                aState = AnimState.Right;
                source = new Rectangle(144, 0, frameWidth, frameHeight);
            }
            else if (kState.IsKeyDown(Keys.W) && pkState.IsKeyDown(Keys.W))
            {
                aState = AnimState.Upwalk;
            }
            else if (kState.IsKeyDown(Keys.A) && pkState.IsKeyDown(Keys.A))
            {
                aState = AnimState.Leftwalk;
            }
            else if(kState.IsKeyDown(Keys.S) && pkState.IsKeyDown(Keys.S))
            {
                aState = AnimState.Downwalk;
            }
            else if (kState.IsKeyDown(Keys.D) && pkState.IsKeyDown(Keys.D))
            {
                aState = AnimState.Rightwalk;
            }
            else if (aState == AnimState.Upwalk && !kState.IsKeyDown(Keys.W))
            {
                aState = AnimState.Up;
                source = new Rectangle(48, 0, frameWidth, frameHeight);
            }
            else if (aState == AnimState.Leftwalk && !kState.IsKeyDown(Keys.A))
            {
                aState = AnimState.Left;
                source = new Rectangle(96, 0, frameWidth, frameHeight);
            }
            else if (aState == AnimState.Downwalk && !kState.IsKeyDown(Keys.S))
            {
                aState = AnimState.Down;
                source = new Rectangle(0, 0, frameWidth, frameHeight);
            }
            else if (aState == AnimState.Rightwalk && !kState.IsKeyDown(Keys.D))
            {
                aState = AnimState.Right;
                source = new Rectangle(144, 0, frameWidth, frameHeight);
            }
        }

        public void Draw(GameTime gameTime)
        {
            // Process elapsed time
            time += (float)gameTime.ElapsedGameTime.TotalSeconds;
            while (time > frameTime)
            {
                // Play the next frame in the SpriteSheet
                frameIndex++;

                // reset elapsed time
                time = 0f;
            }
            if (frameIndex >= totalFrames) frameIndex = 0;

            //int frameIndex = 0;
            if (aState == AnimState.Downwalk)
            {
                source = new Rectangle(0, (frameIndex * 2) * frameHeight, frameWidth, frameHeight);
            }

            else if (aState == AnimState.Upwalk)
            {
                source = new Rectangle(48, (frameIndex * 2) * frameHeight, frameWidth, frameHeight);
            }

            else if (aState == AnimState.Leftwalk)
            {
                source = new Rectangle(96, (frameIndex * 2) * frameHeight, frameWidth, frameHeight);
            }

            else if (aState == AnimState.Rightwalk)
            {
                source = new Rectangle(144, (frameIndex * 2) * frameHeight, frameWidth, frameHeight);
            }

            else if (aState == AnimState.Down)
            {
                source = new Rectangle(0, 0, frameWidth, frameHeight);
            }

            else if (aState == AnimState.Up)
            {
                source = new Rectangle(48, 0, frameWidth, frameHeight);
            }

            else if (aState == AnimState.Left)
            {
                source = new Rectangle(96, 0, frameWidth, frameHeight);
            }

            else if (aState == AnimState.Right)
            {
                source = new Rectangle(144, 0, frameWidth, frameHeight);
            }
        }

        public bool SingleKeyPress(Keys key, KeyboardState kState, KeyboardState pkState)
        {
            if (kState.IsKeyDown(key))
            {
                if (pkState.IsKeyUp(key))
                    return true;
                else
                    return false;
            }
            else
                return false;
        }
    }
}
