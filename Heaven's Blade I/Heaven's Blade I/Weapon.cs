﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Heaven_s_Blade_I
{
    class Weapon : Item 
    {
        WType weaponType;
        WProperties prop1;
        WProperties prop2;

        Properties propLists = new Properties();

        public Weapon()
        {
            weaponType = GenerateType();
            GenerateProperty();
        }

        private void GenerateProperty()
        {
            prop1 = propLists.weaponProps[rand.Next(0, propLists.weaponProps.Count)];

            if ((weaponType != WType.Mace && weaponType != WType.Warhammer) && prop1 == WProperties.Meteoric)
            {
                GenerateProperty();
            }

            if ((weaponType == WType.Mace || weaponType == WType.Warhammer) && prop1 == WProperties.Bloody)
            {
                GenerateProperty();
            }

            if ((weaponType == WType.Mace || weaponType == WType.Warhammer) && prop1 == WProperties.Poisoned)
            {
                GenerateProperty();
            }
            
            prop2 = propLists.weaponProps[rand.Next(0, propLists.weaponProps.Count)];

            if (prop2 == prop1)
            {
                GenerateProperty();
            }

            if ((weaponType != WType.Mace && weaponType != WType.Warhammer) && prop2 == WProperties.Meteoric)
            {
                GenerateProperty();
            }

            if ((weaponType == WType.Mace || weaponType == WType.Warhammer) && prop2 == WProperties.Bloody)
            {
                GenerateProperty();
            }

            if ((weaponType == WType.Mace || weaponType == WType.Warhammer) && prop2 == WProperties.Poisoned)
            {
                GenerateProperty();
            }

            if (prop1 == WProperties.Divine || prop1 == WProperties.Daemonic)
            {
                GenerateProperty();
            }
        }

        private WType GenerateType()
        {
            return propLists.weaponType[rand.Next(0, propLists.weaponType.Count)];
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2}", prop1, prop2, weaponType);
        }
    }
}
