﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;

namespace Heaven_s_Blade_I
{
    class EdenAngel : Enemy
    {
        public Vector2 evec;
        public EnemyAnimate EA;
        public double angle;
        public int dtp;
        
        public EdenAngel(int x, int y, Texture2D Image)
            : base(x, y, Image)
        {
            prect = new Rectangle(x, y, 32, 48);
            image = Image;
            evec = new Vector2(x, y);
            EA = new EnemyAnimate(500, 300, Image);
        }

        public override void Range(Vector2 pvec)
        {
            dtp = (int)Math.Sqrt((Math.Pow((Math.Abs(pvec.X - evec.X)), 2)) + (Math.Pow((Math.Abs(pvec.Y - evec.Y)), 2)));
        }

        public override void Track(Vector2 pvec)
        {
            angle = Math.Atan((pvec.Y - evec.Y)/(evec.X - pvec.X));
            if((angle > -Math.PI/4 && angle < Math.PI/4) && (pvec.X + 8) < evec.X)
            {
                EA.aState = AnimState.Left;
            }
            else if ((angle < -Math.PI / 4 || angle > Math.PI / 4) && (pvec.Y + 12) < evec.Y)
            {
                EA.aState = AnimState.Up;
            }
            else if ((angle > -Math.PI / 4 && angle < Math.PI / 4) && (pvec.X + 8) > evec.X)
            {
                EA.aState = AnimState.Right;
            }
            else if ((angle < -Math.PI / 4 || angle > Math.PI / 4) && (pvec.Y + 12) > evec.Y)
            {
                EA.aState = AnimState.Down;
            }
        }
      
        public override void Follow(AnimState aState)
        {
            if (aState == AnimState.Leftwalk)
            {
                evec.X -= 2;
            }
            else if (aState == AnimState.Upwalk)
            {
                evec.Y -= 2;
            }
            else if (aState == AnimState.Rightwalk)
            {
                evec.X += 2;
            }
            else if (aState == AnimState.Downwalk)
            {
                evec.Y += 2;
            }
            else if (aState == AnimState.Left)
            {
                evec.X -= 2;
                EA.aState = AnimState.Leftwalk;
            }
            else if (aState == AnimState.Up)
            {
                evec.Y -= 2;
                EA.aState = AnimState.Upwalk;
            }
            else if (aState == AnimState.Right)
            {
                evec.X += 2;
                EA.aState = AnimState.Rightwalk;
            }
            else if (aState == AnimState.Down)
            {
                evec.Y += 2;
                EA.aState = AnimState.Downwalk;
            }
            
        }
    }
}
