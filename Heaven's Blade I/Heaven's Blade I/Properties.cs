﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Heaven_s_Blade_I
{   
    public enum WProperties
    {
        Flaming,
        Frost,
        Poisoned,
        Gust,
        Shocking,
        Explosive,
        Meteoric,
        Bloody,
        Vampiric,
        Lichbane,
        Lycanbane,
        Daemonbane,
        Darkbane,
        Manbane,
        Daemonic,
        Divine //16
    }

    public enum WType
    {
        Sword,
        Longsword,
        Staff,
        Spear,
        Mace,
        Warhammer,
        Axe,
        Greataxe,
    }
    class Properties
    {
        public List<WProperties> weaponProps;
        public List<WType> weaponType;

        public Random propGen;

        public Properties()
        {
            propGen = new Random();

            weaponProps = new List<WProperties>();
            weaponType = new List<WType>();

            WeaponProperties();
            WeaponTypes();
        }

        //public WProperties WepPropGen()
        //{
        //   
        //}

        public void WeaponProperties()
        {
            weaponProps.Add(WProperties.Flaming);
            weaponProps.Add(WProperties.Frost);
            weaponProps.Add(WProperties.Poisoned);
            weaponProps.Add(WProperties.Gust);
            weaponProps.Add(WProperties.Shocking);
            weaponProps.Add(WProperties.Explosive);
            weaponProps.Add(WProperties.Meteoric);
            weaponProps.Add(WProperties.Bloody);
            weaponProps.Add(WProperties.Vampiric);
            weaponProps.Add(WProperties.Lichbane);
            weaponProps.Add(WProperties.Lycanbane);
            weaponProps.Add(WProperties.Daemonbane);
            weaponProps.Add(WProperties.Darkbane);
            weaponProps.Add(WProperties.Manbane);
            weaponProps.Add(WProperties.Daemonic);
            weaponProps.Add(WProperties.Divine); //16
        }

        public void WeaponTypes()
        {
            weaponType.Add(WType.Sword);
            weaponType.Add(WType.Longsword);
            weaponType.Add(WType.Staff);
            weaponType.Add(WType.Spear);
            weaponType.Add(WType.Mace);
            weaponType.Add(WType.Warhammer);
            weaponType.Add(WType.Axe);
            weaponType.Add(WType.Greataxe);
        }
    }
}
