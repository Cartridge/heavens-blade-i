﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;

namespace Heaven_s_Blade_I
{
    class Player
    {
        // the spritesheet containing our animation frames
        public Texture2D image;

        public Rectangle prect;

        public Vector2 pvec;

        public Player(int x, int y, Texture2D Image)
        {
            prect = new Rectangle(x, y, 32, 48);
            image = Image;
            pvec = new Vector2(x, y);
        }

        public void Update(KeyboardState kState, KeyboardState pkState)
        {
            if (kState.IsKeyDown(Keys.W))
            {
                pvec.Y-=4;
            }
            else if (kState.IsKeyDown(Keys.A))
            {
                pvec.X-=4;
            }
            else if (kState.IsKeyDown(Keys.S))
            {
                pvec.Y+=4;
            }
            else if (kState.IsKeyDown(Keys.D))
            {
                pvec.X+=4;
            }
        }

        public bool SingleKeyPress(Keys key, KeyboardState kState, KeyboardState pkState)
        {
            if (kState.IsKeyDown(key))
            {
                if (pkState.IsKeyUp(key))
                    return true;
                else
                    return false;
            }
            else
                return false;
        }
    }
}
