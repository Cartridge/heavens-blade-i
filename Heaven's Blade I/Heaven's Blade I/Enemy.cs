﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;

namespace Heaven_s_Blade_I
{
    class Enemy : Player
    {       
        public Enemy(int x, int y, Texture2D Image)
            : base(x, y, Image)
        {
            prect = new Rectangle(x, y, 32, 48);
            image = Image;
            pvec = new Vector2(x, y);
        }

        public virtual void Track(Vector2 pvec)
        {
        }

        public virtual void Follow(AnimState aState)
        {
        }

        public virtual void Range(Vector2 pvec)
        {
        }
    }
}
