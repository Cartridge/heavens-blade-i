﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Threading;
using System.IO; 
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
#endregion

namespace Heaven_s_Blade_I
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        SpriteFont font;
        Weapon weapon = new Weapon();
        List<Weapon> armory = new List<Weapon>();

        int i = 0;
        int time = 0;

        Player player;
        PlayerAnimate pAnim;
        Texture2D pImage;

        Enemy e1;
        EnemyAnimate eAnim;
        Texture2D eImage;

        EdenAngel ea1;
        Texture2D eaImage;

        public KeyboardState kState;
        public KeyboardState pkState;
        KeyboardState keyS;

        public Game1()
            : base()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            font = this.Content.Load<SpriteFont>("SpriteFont1");
            pImage = this.Content.Load<Texture2D>("Textures/abaddonsprites");
            eImage = this.Content.Load<Texture2D>("Textures/edenangel1sprites");
            eaImage = this.Content.Load<Texture2D>("Textures/edenangel1sprites");
            
            player = new Player(300, 300, pImage);
            pAnim = new PlayerAnimate(300, 300, pImage);
            e1 = new Enemy(500, 300, eImage);
            eAnim = new EnemyAnimate(500, 300, eImage);
            ea1 = new EdenAngel(512, 312, eaImage);

            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            pkState = kState;
            kState = Keyboard.GetState();

            keyS = Keyboard.GetState();

            while (i < 10)
            {
                armory.Add(new Weapon());
                i++;
                Thread.Sleep(15); 
            }

            pAnim.AUpdate(kState, pkState);

            player.Update(kState, pkState);

            ea1.Track(player.pvec);

            ea1.Range(player.pvec);

            //eAnim.aState = ea1.EA.aState;

            if (ea1.dtp <= 150)
            {
                ea1.Follow(ea1.EA.aState);
            }
            if (ea1.dtp > 150)
            {
                if(ea1.EA.aState == AnimState.Leftwalk)
                {
                    ea1.EA.aState = AnimState.Left;
                }
                if (ea1.EA.aState == AnimState.Upwalk)
                {
                    ea1.EA.aState = AnimState.Up;
                }
                if (ea1.EA.aState == AnimState.Rightwalk)
                {
                    ea1.EA.aState = AnimState.Right;
                }
                if (ea1.EA.aState == AnimState.Downwalk)
                {
                    ea1.EA.aState = AnimState.Down;
                }
            }

            //eAnim.AUpdate(kState, pkState);

            //e1.Update(kState, pkState);

            //if (kState.IsKeyDown(Keys.W) || kState.IsKeyDown(Keys.A) || kState.IsKeyDown(Keys.S) || kState.IsKeyDown(Keys.D))
            //{
            //    eAnim.aState = pAnim.aState;
            //}

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            spriteBatch.Begin();

            Vector2 vec = new Vector2(0,0); 

            foreach (Weapon wep in armory)
            {
                spriteBatch.DrawString(font, wep.ToString(), vec, Color.White);
                vec.Y += 20; 
            }

            spriteBatch.DrawString(font, string.Format("{0}", ea1.angle), new Vector2(500, 0), Color.White);
            spriteBatch.DrawString(font, string.Format("{0}", ea1.dtp), new Vector2(500, 30), Color.White);

            if (ea1.EA.aState == AnimState.Left)
            {
                spriteBatch.DrawString(font, "Left", new Vector2(200, 0), Color.White);
            }
            else if (ea1.EA.aState == AnimState.Up)
            {
                spriteBatch.DrawString(font, "Up", new Vector2(200, 0), Color.White);
            }
            else if (ea1.EA.aState == AnimState.Right)
            {
                spriteBatch.DrawString(font, "Right", new Vector2(200, 0), Color.White);
            }
            else if (ea1.EA.aState == AnimState.Down)
            {
                spriteBatch.DrawString(font, "Down", new Vector2(200, 0), Color.White);
            }
            else if (ea1.EA.aState == AnimState.Leftwalk)
            {
                spriteBatch.DrawString(font, "Leftwalk", new Vector2(200, 0), Color.White);
            }
            else if (ea1.EA.aState == AnimState.Upwalk)
            {
                spriteBatch.DrawString(font, "Upwalk", new Vector2(200, 0), Color.White);
            }
            else if (ea1.EA.aState == AnimState.Rightwalk)
            {
                spriteBatch.DrawString(font, "Rightwalk", new Vector2(200, 0), Color.White);
            }
            else if (ea1.EA.aState == AnimState.Downwalk)
            {
                spriteBatch.DrawString(font, "Downwalk", new Vector2(200, 0), Color.White);
            }

            //spriteBatch.Draw(player.image, player.pvec, pAnim.source, Color.White);
            
            if (pAnim.aState == AnimState.Downwalk || pAnim.aState == AnimState.Upwalk ||
                pAnim.aState == AnimState.Leftwalk || pAnim.aState == AnimState.Rightwalk)
            {
                pAnim.Draw(gameTime);
            }

            ea1.EA.Draw(gameTime);

            spriteBatch.Draw(player.image, player.pvec, pAnim.source, Color.White, 0, new Vector2(0, 0), 2, SpriteEffects.None, 0);

            spriteBatch.Draw(ea1.image, ea1.evec, ea1.EA.source, Color.White, 0, new Vector2(0, 0), 2, SpriteEffects.None, 0);

            spriteBatch.End();

            base.Draw(gameTime);
        }

        public bool SingleKeyPress(Keys key)
        {
            if (kState.IsKeyDown(key))
            {
                if (pkState.IsKeyUp(key))
                    return true;
                else
                    return false;
            }
            else
                return false;
        }
    }
}
